<?php
if(isset($_GET['d']) && isset($_GET['i'])) {
	//header('Content-Type: application/json');
	header('Content-Type: text/json');
	$file = fopen('/proc/net/dev', 'r');

	$lines = array();
	while(!feof($file))
		$lines[] = trim(fgets($file));
	fclose($file);
	array_pop($lines);	 // remove empty line at end

	$interval = intval($_GET['i']);
	foreach($lines as $i=>$line) {
		if($i<3) continue;
		
		$parts = explode(':', $line);
		$iface = trim($parts[0]);
		preg_match_all('/\d{1,64}/', $parts[1], $data);
		$rx = ceil($data[0][0]/1024); // byte to kilobyte
		$tx = ceil($data[0][8]/1024); // byte to kilobyte
		echo "{\"i\":\"{$iface}\", \"r\":{$rx}, \"t\":{$tx}}";
		break;
	}
	die();
} elseif(isset($_GET['i'])) {
	$interval = $_GET['i'];
} else {
	$interval = 1000;
}
?>

<!DOCTYPE html>
<html>
	<head>
			<style>
				p {
					margin: 2px 0;
					font-family: monospace;
					font-size: 12px;
					
				}
			</style>
	</head>
	<body>
		<div>
			<canvas id="graph" width="600" height="200"></canvas>
			<p>Down : <span id="down">0</span> KB/s</p>
			<p>Up&nbsp;&nbsp; : <span id="up">0</span> KB/s</p>
		</div>
		<script src="jquery.min.js" type="text/javascript"></script>
		<script type="text/javascript" src="smoothie.js"></script>
		<script type="text/javascript">
			var interval = <?php echo $interval; ?>;
			
			var resp = $.ajax({
				type: 'get',       
				url: "index.php?d=1&i="+interval,
				dataType: 'jsonp',
				context: document.body,
				global: false,
				async:false,
				success: function(data) {
					return data;
				}
			}).responseText;
			data = JSON.parse(resp);
			var oldRx = data.r;
			var oldTx = data.t;

			var smoothie = new SmoothieChart({
				preMaxLabel: 'Max: ',
				preMinLabel: 'Min: ',
				postMaxLabel: ' KB/s',
				postMinLabel: ' KB/s',
				minValue: 0,       
				millisPerPixel: interval, // sets the speed at which the chart pans by
				interpolation: 'linear',
				grid: { verticalSections: 0 },
				labels: { fillStyle:'rgb(255, 255, 0)' }
			});
			smoothie.streamTo(document.getElementById("graph"));
			
			var rxLine = new TimeSeries();
			var txLine = new TimeSeries();
			
			var upLabel = $('#up');
			var downLabel = $('#down');
			
			setInterval(function() {
				var resp = $.ajax({
					type: 'get',       
					url: "index.php?d=1&i="+interval,
					dataType: 'jsonp',
					context: document.body,
					global: false,
					async:false,
					success: function(data) {
						return data;
					}
				}).responseText;
				
				data = JSON.parse(resp);
				tx = (data.t-oldTx)/interval*1000
				rx = (data.r-oldRx)/interval*1000
				
				rxLine.append(new Date().getTime(), rx);
				txLine.append(new Date().getTime(), tx);
				upLabel.text(tx);
				downLabel.text(rx);
				
				oldRx = data.r;
				oldTx = data.t;
				
			}, interval);
			
			smoothie.addTimeSeries(rxLine, { strokeStyle:'rgb(0, 255, 0)', fillStyle:'rgba(0, 255, 0, 0.2)', lineWidth: 1 });
			smoothie.addTimeSeries(txLine, { strokeStyle:'rgb(0, 0, 255)', fillStyle:'rgba(0, 0, 255, 0.2)', lineWidth: 1 });
			
		</script>	
	</body>
</html>
